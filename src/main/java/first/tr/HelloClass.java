package first.tr;

/**
 * <h1>second homework</h1>
 * @author  Andriy Mota
 * @version 1.0
 * @since   2019-11-07
 */

/**
 * My first Maven project written in IntelliJ IDEA<br>
 * To run main method user have to enter three values;<br>
 * First is starting value of range;<br>
 * Second is ending value of range;<br>
 * Third is quantity of Fibonacci numbers;<br>
 */
public class HelloClass {
/**
* This method prints all odd numbers in range [begin; end])
*
* @param beg int number to start with
* @param end int number to end with
*/
    public void printOdd(final int beg, final int end) {
        System.out.print("Odd numbers: ");
        for (int i = beg; i <= end; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }
    /**
     * This method prints all even numbers in range [begin; end])
     *
     * @param beg int number to start with
     * @param end int number to end with
     */
    public void printEven(final int beg, final int end) {
        System.out.print("Even numbers: ");
        for (int i = end; i >= beg; i--) {
            if ((i % 2 == 0) && (i != 0)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }
    /**
     * This method counts sum for all odd numbers in range [begin; end])
     *
     * @param beg int number to start with
     * @param end int number to end with
     * @return sum of odd numbers in stated range
     */
    public int getOddSum(final int beg, final int end) {
        int oddSum = 0;
        for (int i = beg; i <= end; i++) {
            if (i % 2 != 0) {
                oddSum += i;
            }
        }
        return oddSum;
    }
    /**
     * This method counts sum for all even numbers in range [begin; end])
     *
     * @param beg int number to start with
     * @param end int number to end with
     * @return sum of even numbers in stated range
     */
    public int getEvenSum(final int beg, final int end) {
        int evenSum = 0;
        for (int i = end; i >= beg; i--) {
            if (i % 2 == 0) {
                evenSum += i;
            }
        }
        return evenSum;
    }
    /**
     * This method prints and counts Fibonacci numbers starting from 1
     * This method also prints percentage of odd and even Fibonacci
     * numbers
     *
     * @param size count of Fibonacci numbers you want
     *             to print on screen
     * @return arrays of ints with only 2 elements, where int[0]
     * is the largest Odd number
     * and int[1] is the largest even number
     */
    public int[] getFiboNumbs(final int size) {
        int[] result = new int[2];
        int F1 = -1;         //largest odd; variable is named according to task
        int F2 = -1;         //largest even; variable is named according to task
        int oddVal = 0;
        int evenVal = 0;
        float precOdd = 0;
        float precEven = 0;
        int cur = 1;
        int prev = 0;
        int tmp;
        for (int i = 0; i < size; i++) {
            if (cur % 2 != 0) {
                F1 = cur;
                oddVal += 1;
            } else {
                F2 = cur;
                evenVal += 1;
            }
            System.out.print(cur + " ");
            tmp =  cur;
            cur = tmp + prev;
            prev = tmp;
        }
        precOdd = ((float) (oddVal) / (size)) * 100;
        precEven = 100 - precOdd;
        result[0] = F1;
        result[1] = F2;
        System.out.println();
        System.out.println("F1 = " + F1);
        System.out.println("F2 = " + F2);
        System.out.println("precOdd = " + precOdd);
        System.out.println("precEven = " + precEven);
        return result;
    }
    /**
     * This is the entry point of my homework
     *
     * @param args User can enter the interval and quantity
     *            of Fibonacci numbers to generate
     *            so he has to enter THREE arguments, or
     *            default parameters is used
     *            interval: [0; 50]
     *            Fibonacci numbers quantity: 8
     */
    public static void main(String[] args) {
        int beg;
        int end;
        int fiboSize;
        final int defaultBeg = 0;
        final int defaultEnd = 50;
        final int defaultFiboSize = 8;
        int oddSum = 0;
        int evenSum = 0;
        switch (args.length) {
            case 1: {
                beg = Integer.parseInt(args[0]);
                end = defaultEnd;
                fiboSize = defaultFiboSize;
                break;
            }
            case 2: {
                beg = Integer.parseInt(args[0]);
                end = Integer.parseInt(args[1]);
                fiboSize = defaultFiboSize;
                break;
            }
            case 3: {
                beg = Integer.parseInt(args[0]);
                end = Integer.parseInt(args[1]);
                fiboSize = Integer.parseInt(args[2]);
                break;
            }
            default: {
                beg = defaultBeg;
                end = defaultEnd;
                fiboSize = defaultFiboSize;
            }
        }
        if (end <= beg) {
            System.out.println("End <= beg!");
            System.exit(0);
        } else if (fiboSize <= 0) {
            System.out.println("Fibonacci numbers quantity <= 0!");
            System.exit(0);
        }
        HelloClass hc = new HelloClass();
        hc.printEven(beg, end);
        hc.printOdd(beg, end);
        evenSum = hc.getEvenSum(beg, end);
        oddSum = hc.getOddSum(beg, end);
        System.out.println("evenSum = " + evenSum);
        System.out.println("oddSum = " + oddSum);
        int[] res = hc.getFiboNumbs(fiboSize);
        System.out.println();
    }
}